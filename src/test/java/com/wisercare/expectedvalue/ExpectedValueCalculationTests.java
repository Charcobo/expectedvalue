package com.wisercare.expectedvalue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.util.AssertionErrors.assertFalse;
import static org.springframework.test.util.AssertionErrors.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;

class ExpectedValueCalculationTests {

	private ExpectedValueCalculator expectedValueCalculator = new ExpectedValueCalculator();

	@Test
	void exampleExpectedValueCalculation()
	{
		//Act One : Setup

		// define some outcomes... these could be benefits or side effects caused by treatment options
		// in this example, they are all negative side effects
		// we'll determine how much this theoretical candidate cares about these possible side effects below.
		Outcome addiction = new Outcome("Drug Dependency or Addiction",
				"Some sleep medications have a greater risk of causing addiction than others");

		Outcome drowsiness = new Outcome("Daytime Drowsiness", "Feeling extra tired during the daytime");

		Outcome sleepwalking = new Outcome("Sleepwalking",
				"Sleepwalking, sleep-eating or other abnormal nighttime behaviors.");

		//define some treatment options that may be considered for this patient's condition
		//in a real scenario, this list could be filtered against treatments that are not appropriate
		//for this particular patient
		Treatment ambien = new Treatment("Ambien",
				"Generic Name: Zolpidem immediate-release. Somewhat addictive. Frequently causes sleepwalking");

		Treatment rozerem = new Treatment("Rozerem",
				"Generic name : Ramelton. Non addictive. Makes you sleepy.");

		Treatment sonata = new Treatment("Sonata",
				"Generic name : Zapelon. Highly addictive. almost never causes drowsiness or sleepwalking");

		List<Treatment> treatments = Arrays.asList(ambien, rozerem, sonata);

		//fill out the grid of probabilities... how likely is each treatment option to lead to each outcome?
		// 1.0 == this outcome will ALWAYS Happen as a result of this treatment.
		// 0.5 == this outcome has a 50% CHANCE of happening. It's a coin toss.
		// 0.0 == this outcome will NEVER Happen as a result of this treatment.
		//in a real scenario, these probabilities are often customized to reflect a particular patient's clinical profile

		ProbabilityMap probabilityMap = new ProbabilityMap();
		probabilityMap.addProbability(ambien, addiction, 0.2); //sometimes leads to addiction
		probabilityMap.addProbability(ambien, drowsiness, 0.0); //never causes drowsiness
		probabilityMap.addProbability(ambien, sleepwalking, 0.8); //frequently causes sleepwalking

		probabilityMap.addProbability(rozerem, addiction, 0.0); //non-addictive
		probabilityMap.addProbability(rozerem, drowsiness, 1.0); //always causes drowsiness
		probabilityMap.addProbability(rozerem, sleepwalking, 0.1); //rarely causes sleepwalking

		probabilityMap.addProbability(sonata, addiction, 0.6); //relatively high chance of addiction
		probabilityMap.addProbability(sonata, drowsiness, 0.01); //sometimes causes daytime drowsiness
		probabilityMap.addProbability(sonata, sleepwalking, 0.01); ///almost never causes sleepwalking

		//set the relative preferences of these possible outcomes
		//higher values include things that are more important for the patient
		//lower values indicate possible outcomes that are less important to this patient
		//in the real application these values are determined from tradeoff questions or preference sliders.
		List<PatientOutcomePreference> patientOutcomePreferences = new ArrayList<>();

		// our patient is most concerned about getting addicted to insomnia medication,
		// sleepwalking is a close second and daytime drowsiness is a distant third.
		// as these outcomes are all considered bad outcomes, they are presented as negative numbers
		patientOutcomePreferences.add(new PatientOutcomePreference(addiction, -.6));
		patientOutcomePreferences.add(new PatientOutcomePreference(sleepwalking, -.3));
		patientOutcomePreferences.add(new PatientOutcomePreference(drowsiness, -.1));

		//Act Two : Execute
		//this list of expected values is used to display treatment fit to patients in the experience, and care providers in PDF reports
		List<TreatmentExpectedValue> expectedValues = expectedValueCalculator.processValuesForTreatments(treatments, patientOutcomePreferences, probabilityMap);

		//Act Three : Verify
		assertNotNull(expectedValues, "We should get a non-null list of expected values");
		assertEquals(3, expectedValues.size(), "we expect as many expected values as we have treatment options");

		TreatmentExpectedValue bestAlignedOption = expectedValues.get(0);
		TreatmentExpectedValue secondBestAlignedOption = expectedValues.get(1);
		TreatmentExpectedValue thirdBestAlignedOption = expectedValues.get(2);


		assertTrue("the list of expected values should be presented in descending order",
				bestAlignedOption.getExpectedValue() > secondBestAlignedOption.getExpectedValue()
				&& secondBestAlignedOption.getExpectedValue() > thirdBestAlignedOption.getExpectedValue());

		assertFalse("the first and second best ranked treatments should be distinct", bestAlignedOption.getTreatment().equals(secondBestAlignedOption.getTreatment()));
		assertFalse("the second and last ranked treatments should be distinct", secondBestAlignedOption.getTreatment().equals(thirdBestAlignedOption.getTreatment()));


		assertTrue("The non-addictive rozerem is most aligned with this patient's preferences", bestAlignedOption.getTreatment().equals(rozerem));
		assertTrue("Ambien is a decent second choice", secondBestAlignedOption.getTreatment().equals(ambien));
		assertTrue("sonata is a bad idea", thirdBestAlignedOption.getTreatment().equals(sonata));
	}
}
