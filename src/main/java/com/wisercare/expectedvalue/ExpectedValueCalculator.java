package com.wisercare.expectedvalue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExpectedValueCalculator
{

    public List<TreatmentExpectedValue> processValuesForTreatments(
            List<Treatment> treatments,
            List<PatientOutcomePreference> patientOutcomePreferences,
            ProbabilityMap probabilityMap)
    {
        /*
        with the information passed in to this method, it's possible to calculate an "Expected Value"

        which is the sum of all of the individual results of
          multiplying each outcome preference value (how much the patient cares about a thing)
          with that outcome's probability in relation to a treatment (how likely the thing is to happen)

        //Example: if you have 3 possible outcomes (A,B,C) for treatment, the expected value would be:
           (chance of outcome A * preference of outcome A)
         + (chance of outcome B * preference of outcome B)
         + (chance of outcome C * preference of outcome C)

        These treatment expected values should be sorted in descending order, with the treatment that will provide the
        greatest relative value displayed first, and the treatment that will provide the least expected value last.
        */


        List<TreatmentExpectedValue> results = new ArrayList<>();

        // For each treatment
        for (Treatment treatment : treatments) {
            Double totalExpectedValue = 0.00;

            // And also for each side effect, multiply the probability of the side effect by the preference for it
            for (PatientOutcomePreference preference : patientOutcomePreferences) {

                // Get which outcome associated with the patient's preference
                Outcome outcome = preference.getOutcome();

                // Get the preference value for a particular outcome
                Double preferenceValue = preference.getPreferenceValue();

                // Get the probability for that outcome
                Double probability = probabilityMap.getPercentChance(treatment, outcome);

                // Calculate the expected value
                Double expectedValue = (preferenceValue * probability);

                // Add all those values together
                totalExpectedValue += expectedValue;
            }

            // Pass the treatment and the value to the TreatmentExpectedValue constructor
            TreatmentExpectedValue treatmentExpectedValue = new TreatmentExpectedValue(treatment, totalExpectedValue);

            // Add the resulting TreatmentExpectedValue to the list
            results.add(treatmentExpectedValue);
        }

        // Sort the list with best treatment listed first (reversing the natural order)
        Collections.sort(results, Collections.reverseOrder());

        return results;
    }
}
