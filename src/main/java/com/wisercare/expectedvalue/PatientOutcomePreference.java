package com.wisercare.expectedvalue;

import lombok.Data;

@Data
public class PatientOutcomePreference
{
    private Outcome outcome;
    private Double preferenceValue; //how much does this patient care about this particular outcome

    public PatientOutcomePreference(Outcome outcome, Double preferenceValue) {
        this.outcome = outcome;
        this.preferenceValue = preferenceValue;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public String getOutcomeName() {
        return outcome.getName();
    }

    public Double getPreferenceValue() {
        return preferenceValue;
    }
}
