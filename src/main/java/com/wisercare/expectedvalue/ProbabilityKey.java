package com.wisercare.expectedvalue;


import lombok.Data;

@Data
public class ProbabilityKey {
    private Treatment treatment;
    private Outcome outcome;

    public ProbabilityKey(Treatment treatment, Outcome outcome) {
        this.treatment = treatment;
        this.outcome = outcome;
    }
}
