package com.wisercare.expectedvalue;

import lombok.Data;

@Data
//a treatment is some sort of medical intervention (or non-intervention) for a particular condition
//treatments are related to potential outcomes through their probabilities
public class Treatment {
    private String name;
    private String description;

    public Treatment(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
