package com.wisercare.expectedvalue;

import lombok.Data;

@Data
public class TreatmentExpectedValue implements Comparable<TreatmentExpectedValue>
{
    private Treatment treatment;

    //this is a type of "score" that indicates how well the treatment aligns with what the patient cares about
    private Double expectedValue;

    public TreatmentExpectedValue(Treatment treatment, Double expectedValue) {
        this.treatment = treatment;
        this.expectedValue = expectedValue;
    }

    public Treatment getTreatment() {
        return treatment;
    }

    public Double getExpectedValue() {
        return expectedValue;
    }

    public int compareTo(TreatmentExpectedValue treatmentExpectedValue) {
        // Get this current expectedValue
        Double thisExpectedValue = this.expectedValue;

        // Get the expectedValue with which to compare
        Double comparedExpectedValue = treatmentExpectedValue.getExpectedValue();

        int result = thisExpectedValue.compareTo(comparedExpectedValue);

        // return the result, but take into account drugs that have equal effects (not part of this test)
        if (result == 0) {
            return treatment.getName().compareTo(treatmentExpectedValue.getTreatment().getName());
        } else {
            return result;
        }
    }
}
